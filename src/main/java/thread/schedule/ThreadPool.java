package thread.schedule;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @PROJECT_NAME: counter
 * @DESCRIPTION: 应用场景：在指定时间给大家发消息【将消息（包括发送时间）存储在数据库中，使用定时任务每隔1s检查数据库在当前时间有没有需要发送的消息。】
 * @USER: 涂玄武
 * @DATE: 2020/11/30 11:26
 */
public class ThreadPool {

    private static final ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(1, Executors.defaultThreadFactory());

    private static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
        //新建一个固定延迟时间的计划任务
        executor.scheduleWithFixedDelay(() -> {
            if (haveMsgAtCurrentTime()){
                System.out.println(df.format(new Date()));
                System.out.println("大家注意了，我要发消息了！");
            }
        },1,1, TimeUnit.SECONDS);
    }

    public static boolean haveMsgAtCurrentTime(){
        //查询数据库，有没有当前时间需要发送的消息
        //这里省略实现，直接返回true
        return true;
    }

}
