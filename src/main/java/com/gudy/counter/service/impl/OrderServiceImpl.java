package com.gudy.counter.service.impl;

import com.gudy.counter.bean.res.OrderInfo;
import com.gudy.counter.bean.res.PosiInfo;
import com.gudy.counter.bean.res.TradeInfo;
import com.gudy.counter.config.CounterConfig;
import com.gudy.counter.service.OrderService;
import com.gudy.counter.util.DbUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import thirdpart.order.CmdType;
import thirdpart.order.OrderCmd;
import thirdpart.order.OrderDirection;
import thirdpart.order.OrderType;

import java.util.List;

/**
 * @author 0100064695
 * @PROJECT_NAME: counter
 * @DESCRIPTION:
 * @USER: 涂玄武
 * @DATE: 2020/12/7 13:47
 */
@Component
@Log4j2
public class OrderServiceImpl implements OrderService {

    /**
     * 查资金
     * @param uid
     * @return
     */
    @Override
    public Long getBalance(long uid) {
        return DbUtil.getBanlance(uid);
    }

    /**
     * 查持仓
     * @param uid
     * @return
     */
    @Override
    public List<PosiInfo> getPosiList(long uid) {
        return DbUtil.getPosiList(uid);
    }

    /**
     * 查委托
     * @param uid
     * @return
     */
    @Override
    public List<OrderInfo> getOrderList(long uid) {
        return DbUtil.getOrderList(uid);
    }

    /**
     * 查成交
     * @param uid
     * @return
     */
    @Override
    public List<TradeInfo> getTradeList(long uid) {
        return DbUtil.getTradeList(uid);
    }

    @Autowired
    private CounterConfig config;

    @Override
    public boolean sendOrder(long uid, short type, long timestamp, int code, byte direction, long price, long volume, byte ordertype) {
        final OrderCmd orderCmd = OrderCmd.builder()
                .type(CmdType.of(type))
                .timestamp(timestamp)
                .mid(config.getId())
                .uid(uid)
                .code(code)
                .direction(OrderDirection.of(direction))
                .price(price)
                .volume(volume)
                .orderType(OrderType.of(ordertype))
                .build();

        //1.入库
        int oid = DbUtil.saveOrder(orderCmd);
        if (oid < 0){
            return false;
        } else {
            //TODO：发送网关
            log.info(orderCmd);
        }
        return true;
    }
}
