package com.gudy.counter.bean.res;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 0100064695
 * @PROJECT_NAME: counter
 * @DESCRIPTION: 委托信息
 * @USER: 涂玄武
 * @DATE: 2020/12/4 16:44
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class OrderInfo {

    /**数据库主键*/
    private int id;

    /**账户ID*/
    private long uid;

    /**股票代码*/
    private int code;

    /**股票名称（关联查询）*/
    private String name;

    /**委托股票流通方向*/
    private int direction;

    /**股票类型*/
    private int type;

    /**委托股票单股价格*/
    private long price;

    /**委托股票数量*/
    private long ocount;

    /**委托状态*/
    private int status;

    /**委托日期*/
    private String date;

    /**委托时间*/
    private String time;

}
